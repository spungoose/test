# Ruby 1.9.2 only

def getSize(word)
  "#{word.capitalize}: #{word.length}"
end

words = [ "colors", "drink", "expected", "last" ]

words.each {|word| puts getSize(word)}

a = %w{ ant bee cat dog elk }
puts a[1].each_byte {|c| print c, ' ' }
puts "The array is now #{a.inspect}"

akv = {
  'cello' => 'string',
  'clarinet' => 'woodwind',
  'drum' => 'percussion'
}

p akv['drum']

hkv = {
  oboe: 'woodwind',
  trumpet: 'brass',
  violin: 'string'
}

p hkv[:violin]