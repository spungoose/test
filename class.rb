# Ruby 1.9.2

class BookInStock

  attr_reader   :isbn
  attr_accessor :price

  def initialize(isbn, price)
    @isbn = isbn
    @price = Float(price)
  end
  
  # Method converts object into string
  # Gets invoked by: puts book
  def to_s
    "ISBN: #{@isbn}, price: #{@price}"
  end
  
  # Virtual mappings 
  
  def price_in_cents # reads the price and converts to cents
    Integer(price*100 + 0.5)
  end
  
  def price_in_cents=(cents) # converts cents to price and changes price
    @price = cents / 100.0
  end
  
end

book = BookInStock.new("isbn1", 25.34)
puts book # calles to_s
puts "ISBN = #{book.isbn}"
puts "Price = #{book.price}"

puts "Price in cents = #{book.price_in_cents}" # reads price via virtual mapping

book.price = book.price * 0.75 # sets price via accessor
puts "Discount price = #{book.price.round(2)}"

book.price_in_cents = 1834 # sets price via virtual mapping
puts "Last price = #{book.price}"